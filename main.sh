TIME_STEP=0.002
SIMULATION_LEN=1000


read -p "run equilibriation phase? " yn
case $yn in
    [Yy]* ) bash_scripts/equilibriation_phases.sh > /dev/null 2>&1;;
    [Nn]* ) 
        # cleans up previous simulations and sets up files for this one
        bash_scripts/divide.sh > /dev/null 2>&1
        
        # this is where the configuration for the initial simulations are edited
        python3 python_scripts/edit_config.py simulation/sim_1/md-charmm.mdp $TIME_STEP $SIMULATION_LEN
        python3 python_scripts/edit_config.py simulation/sim_2/md-charmm.mdp $(echo "$TIME_STEP*2" | bc) $SIMULATION_LEN
        
        # run the initial simulation
        echo "initial run" 
        bash_scripts/run_simulations.sh > /dev/null 2>&1
        # compare them
        bash_scripts/compare_simulations.sh > /dev/null 2>&1
        # start the loop
        echo "starting loop"
        bash_scripts/loop_runs.sh
        ;;
    * ) echo "please answer yes or no";;
esac
