import sys

# opens file
f = open(sys.argv[1], "r")

lines = f.readlines()
# looks for timestep
time_step_line = list(filter(lambda char: char != "", lines[4].split(" ")))
time_step = time_step_line[2]
# looks for length of simulation
steps_line = list(filter(lambda char: char != "", lines[5].split(" ")))
steps = steps_line[2]

# apply changes
time_step_line[2] = sys.argv[2]
steps_line[2] = sys.argv[3]
lines[4] = " ".join(time_step_line)
lines[5] = " ".join(steps_line)

new_f = "".join(lines)
f.close()

# save results
f = open(sys.argv[1], "w")
f.write(new_f)
f.close()
