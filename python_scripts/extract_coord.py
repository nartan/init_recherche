import os
import sys
import math
import matplotlib.pyplot as plt

# extracts coordinates from file
def extract_coord(coord_file) :
    coord_list_raw = []
    for line in coord_file :
        coord_list_raw.append(line.strip().split("\t"))

    atoms_traj = [[0] * len(coord_list_raw)] * (len(coord_list_raw[0])//3)
    time_step = 0
    for time_raw in coord_list_raw :
        for k in range(0, len(time_raw)-1, 3) :
            atoms_traj[k//3][time_step] = (float(time_raw[k]), float(time_raw[k+1]), float(time_raw[k+2]))
        time_step += 1
    return atoms_traj


# the distance function used later
def distance(pos_1, pos_2) :
    res = 0
    for i in range(0, 3) :
        res += (pos_1[i] - pos_2[i]) ** 2
    return math.sqrt(res)

# computes the average distance between corresponding atoms of the simulation
def distance_traj(atom_1, atom_2) :
    if len(atom_1) != len(atom_2) :
        print("Warning: the trajectories do not last as long, truncating the longer of the two")
    
    res = []
    for k in range(min(len(atom_1), len(atom_2))) :
        res.append(distance(atom_1[k], atom_2[k]))
    return res

# computes the average distance between corresponding atoms for each step
def compare_coords(traj_1, traj_2) :
    if len(traj_1) != len(traj_2) :
        print("Error: different number of atoms in the trajectories, I refuse to proceed")
        return None


    dist_traj = []
    for k in range(len(traj_1)):
       dist_traj.append(distance_traj(traj_1[k], traj_2[k]))

    average_dist = []
    for i in range(len(dist_traj[0])) :
        avg = 0
        for atom_dist in dist_traj :
            avg += atom_dist[i]
        avg = avg / len(dist_traj)
        average_dist.append(avg)
    return average_dist


# returns the time at which the average distance between two atoms is greater than half of the tolerance
def get_divergence_time(avg_dist, tolerance, steps_per_sample) :
    current_step = 0
    for dist in avg_dist :
        current_step += steps_per_sample
        if dist > tolerance/2 :
            return current_step - steps_per_sample * 2

    return -1


# extract coordinates
coord_file_1 = open(sys.argv[1]).readlines()
coord_file_2 = open(sys.argv[2]).readlines()
traj_1 = extract_coord(coord_file_1)
traj_2 = extract_coord(coord_file_2)

# define tolerance
tolerance = float(sys.argv[3])

# compute average distance between two trajectories
avg_dist = compare_coords(traj_1, traj_2)


# names graph such that it will not overwrite previous graphs
graph_nb = 0
while os.path.isfile("avg_dist_graph_" + str(graph_nb) + ".png") :
    graph_nb += 1
# creates and saves graph only if the user asks
if len(sys.argv) == 5 and sys.argv[4] == "save" :
    plt.plot(avg_dist)
    plt.grid()
    plt.savefig("avg_dist_graph_" + str(graph_nb) + ".png")

print(get_divergence_time(avg_dist, tolerance, 10))
