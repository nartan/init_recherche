Instructions: 
    * Add the .pdb file of a protein file to the input file
    * Rename the file to input.pdb
    * Edit the configuration files if there are specific setting to change
    * Note that the length of the simulation and the step size will be overwritten
    * Execute main.sh, if an equilibriation phase has not been performed yet then run it and on the next execution of main skip it, otherwise, skip it directly
    * The variables for the configuration of the program can be changed in the loop_runs.sh script

The program will execute two simulations in parallel with different timesteps, and progressively reduce it until both simulations are close enough.
The graphs that will show the average distance between the atoms of both simulation over time.
