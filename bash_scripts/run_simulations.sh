cd simulation/sim_1
# prepare the production run
gmx grompp -f md-charmm.mdp -c npt.gro -t npt.cpt -p topol.top -o md.tpr -maxwarn 1
# execute the production run proper
gmx mdrun -ntmpi 1 -v -deffnm md 
# analysis of the results
printf "1\n1\n" | gmx trjconv -s md.tpr -f md.xtc -o md_center.xtc -center -pbc mol
echo "1" | gmx traj -f md_center.xtc -s md.tpr -ox -xvg none
mv coord.xvg coord_1.xvg

cd ../sim_2
# prepare the production run
gmx grompp -f md-charmm.mdp -c npt.gro -t npt.cpt -p topol.top -o md.tpr -maxwarn 1
# execute the production run proper
gmx mdrun -ntmpi 1 -v -deffnm md
# analysis of the results
printf "1\n1\n" | gmx trjconv -s md.tpr -f md.xtc -o md_center.xtc -center -pbc mol
echo "1" | gmx traj -f md_center.xtc -s md.tpr -ox -xvg none
mv coord.xvg coord_2.xvg

