# remove previous simulations
rm -rf simulation/sim_1 simulation/sim_2
# remove previous graphs
rm *.png
# remove files used for comparions of the previous simulations
rm compare_simulations/*
# sets up folders and files for new simulation
mkdir simulation/sim_1 simulation/sim_2
cp simulation/* simulation/sim_1 
cp simulation/* simulation/sim_2
