cp input/* simulation/
cd simulation/
# clean the input structure
grep -v HETATM input.pdb > input_protein_tmp.pdb
grep -v HETATM input_protein_tmp.pdb > input_protein.pdb
rm input_protein_tmp.pdb
# check there are no missing atoms
grep MISSING input.pdb
# generate a topology
gmx pdb2gmx -f input_protein.pdb -o input_processed.gro -water tip3p -ff "charmm27"
# defines the simulation box
gmx editconf -f input_processed.gro -o input_newbox.gro -c -d 1.0 -bt dodecahedron
# fills box with water (once again, depends on the simulation but part of the assumptions)
gmx solvate -cp input_newbox.gro -cs spc216.gro -o input_solv.gro -p topol.top
# creates file for ions
touch ions.mdp
# assembles .tpr file
gmx grompp -f ions.mdp -c input_solv.gro -p topol.top -o ions.tpr
# adds ions in order to make sure that the overal simulation is neutral
printf "SOL\n" | gmx genion -s ions.tpr -o input_solv_ions.gro -conc 0.15 -p \
topol.top -pname NA -nname CL -neutral
# set up energy minimisation
gmx grompp -f emin-charmm.mdp -c input_solv_ions.gro -p topol.top -o em.tpr
# run energy minimisation
gmx mdrun -v -deffnm em
# simulation to find correct temperature, pressure and to equilibrate ions
gmx grompp -f nvt-charmm.mdp -c em.gro -r em.gro -p topol.top -o nvt.tpr
gmx mdrun -ntmpi 1 -v -deffnm nvt
# equilibriate pressure
gmx grompp -f npt-charmm.mdp -c nvt.gro -r nvt.gro -t nvt.cpt -p topol.top -o npt.tpr
gmx mdrun -ntmpi 1 -v -deffnm npt

