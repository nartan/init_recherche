# set up files in order to be able to start from the previous simulation
cd simulation/sim_1
mkdir tmp
mv md.xtc md-charmm.mdp tmp
rm *
cp ../* .
rm md-charmm.mdp md.tpr
mv tmp/* .
rmdir tmp
# assemble the files in preparation for the simulation
gmx grompp -f md-charmm.mdp -p topol.top -c npt.gro -o md.tpr -maxwarn 1
# execute the production run proper
gmx mdrun -ntmpi 1 -v -deffnm md -s md.tpr
# analysis of the results
printf "1\n1\n" | gmx trjconv -s md.tpr -f md.xtc -o md_center.xtc -center -pbc mol
echo "1" | gmx traj -f md_center.xtc -s md.tpr -ox -xvg none
# move the results to the appropriate folder for comparison
mv coord.xvg coord_1.xvg
# cleanup
rm *.cpt *.log em.* *.xtc*


# the same thing as before
cd ../sim_2
mkdir tmp
mv md.tpr md-charmm.mdp tmp
rm *
cp ../* .
rm md-charmm.mdp md.tpr
mv tmp/* .
rmdir tmp
gmx grompp -f md-charmm.mdp -p topol.top -c npt.gro -o md.tpr -maxwarn 1
gmx mdrun -ntmpi 1 -v -deffnm md -s md.tpr
printf "1\n1\n" | gmx trjconv -s md.tpr -f md.xtc -o md_center.xtc -center -pbc mol
echo "1" | gmx traj -f md_center.xtc -s md.tpr -ox -xvg none
mv coord.xvg coord_2.xvg
rm *.cpt *.log em.* *.xtc*

