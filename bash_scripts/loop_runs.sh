END_TIME=1000
CURR_TIME_STEP=0.002
TIME_STEP_LIMIT=0.00001
TOLERANCE=0.1

CUT_TIME=$(python3 python_scripts/extract_coord.py compare_simulations/coord_1.xvg compare_simulations/coord_2.xvg 0.1 save)
#echo "initial cut time"
#echo $CUT_TIME

while [ "$CUT_TIME" -ne "-1" ] && [ $(echo "$TIME_STEP_LIMIT <= $CURR_TIME_STEP" | bc) ];
do
    # the first part of the simulation (nothing particular)
    python3 python_scripts/edit_config.py simulation/sim_1/md-charmm.mdp $CURR_TIME_STEP $CUT_TIME
    python3 python_scripts/edit_config.py simulation/sim_2/md-charmm.mdp $CURR_TIME_STEP $CUT_TIME
    sh bash_scripts/continue_simulation.sh > /dev/null 2>&1

    # the second part of the simulation (with a smaller time step)
    NEXT_END_TIME=$(($CUT_TIME + $END_TIME / 10))
    python3 python_scripts/edit_config.py simulation/sim_1/md-charmm.mdp $(echo "$CURR_TIME_STEP / 2" | bc -l) $(($NEXT_END_TIME > $END_TIME ? $END_TIME : $NEXT_END_TIME))
    python3 python_scripts/edit_config.py simulation/sim_2/md-charmm.mdp $(echo "$CURR_TIME_STEP / 2" | bc -l) $(($NEXT_END_TIME > $END_TIME ? $END_TIME : $NEXT_END_TIME))
    sh bash_scripts/continue_simulation.sh > /dev/null 2>&1

    # the last part of the simulation (back to the regular timestep)
    NEXT_END_TIME=$(($END_TIME - $(($CUT_TIME + $END_TIME / 10))))
    python3 python_scripts/edit_config.py simulation/sim_1/md-charmm.mdp $CURR_TIME_STEP $END_TIME
    python3 python_scripts/edit_config.py simulation/sim_2/md-charmm.mdp $CURR_TIME_STEP $END_TIME
    sh bash_scripts/continue_simulation.sh > /dev/null 2>&1

    # compare the two simulations and figure out when they need to be started from
    mv simulation/sim_1/coord_1.xvg compare_simulations    
    mv simulation/sim_2/coord_2.xvg compare_simulations    
    NEW_CUT_TIME=$(python3 python_scripts/extract_coord.py compare_simulations/coord_1.xvg compare_simulations/coord_2.xvg $TOLERANCE save)

    # either change the timestep or redo part of the simulation with the current smaller timestep
    if (("$NEW_CUT_TIME" <= "$CUT_TIME")) 
    then
        echo "change time step"
        CURR_TIME_STEP=$(echo "$CURR_TIME_STEP / 2" | bc -l)
        # work out when the simulations diverge
        CUT_TIME=$(python3 python_scripts/extract_coord.py compare_simulations/coord_1.xvg compare_simulations/coord_2.xvg $TOLERANCE)
    else
        CUT_TIME=$NEW_CUT_TIME
    fi

    echo "new loop"

done
echo "final time step: $CURR_TIME_STEP"
echo "completely done!"
