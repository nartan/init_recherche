# clean the input structure
grep -v HETATM input.pdb > input_protein_tmp.pdb
grep -v HETATM input_protein_tmp.pdb > input_protein.pdj
rm input_protein_tmp.pdb
# check there are no missing atoms
grep MISSING input.pdb
# generate a topology
gmx pdb2gmx -f input_protein.pdb -o input_processed.gro -water tip3p -ff "charmm27"
# defines the simulation box
gmx editconf -f input_processed.gro -o input_newbox.gro -c -d 1.0 -bt dodecahedron
# fills box with water
gmx solvate -cp input_newbox.gro -cs spc216.gro -o input_solv.gro -p topol.top
# creates file for ions
touch ions.mdp
# assembles .tpr file
gmx grompp -f ions.mdp -c input_solv.gro -p topol.top -o ions.tpr
# adds ions
printf "SOL\n" | gmx genion -s ions.tpr -o input_solv_ions.gro -conc 0.15 -p \
topol.top -pname NA -nname CL -neutral
# set up energy minimisation
gmx grompp -f emin-charmm.mdp -c input_solv_ions.gro -p topol.top -o em.tpr
# run energy minimisation
gmx mdrun -v -deffnm em
# get .xvg file to get info about simulation
printf "Potential\n0\n" | gmx energy -f em.edr -o potential.xvg -xvg none
# show energy evolution, expect a convergence of the potential energy
echo "import pandas as pd; import matplotlib.pyplot as plt; df = pd.read_csv('potential.xvg', sep='\s+', header=None, names=['time','energy']); df.plot('time'); plt.show()" | python3
# simulation to find correct temperature, pressure and to equilibrate ions
gmx grompp -f nvt-charmm.mdp -c em.gro -r em.gro -p topol.top -o nvt.tpr
gmx mdrun -ntmpi 1 -v -deffnm nvt
# show results
echo "Temperature" | gmx energy -f nvt.edr -o temperature.xvg -xvg none
echo "import pandas as pd; import matplotlib.pyplot as plt; df = pd.read_csv('temperature.xvg', sep='\s+', header=None, names=['time','temperature']); df.plot('time'); plt.show()" | python3
# equilibriate pressure
gmx grompp -f npt-charmm.mdp -c nvt.gro -r nvt.gro -t nvt.cpt -p topol.top -o npt.tpr
gmx mdrun -ntmpi 1 -v -deffnm npt
# analyse pressure progression
echo "Pressure" | gmx energy -f npt.edr -o pressure.xvg -xvg none
echo "import pandas as pd; import matplotlib.pyplot as plt; df = pd.read_csv('pressure.xvg', sep='\s+', header=None, names=['time','pressure']); df.plot('time'); plt.show()" | python3
# analyse density using energy
echo "Density" | gmx energy -f npt.edr -o density.xvg -xvg none
echo "import pandas as pd; import matplotlib.pyplot as plt; df = pd.read_csv('density.xvg', sep='\s+', header=None, names=['time','density']); df.plot('time'); plt.show()" | python3
# prepare the production run
gmx grompp -f md-charmm.mdp -c npt.gro -t npt.cpt -p topol.top -o md.tpr
# execute the production run proper
gmx mdrun -ntmpi 1 -v -deffnm md
# analysis of the results
printf "1\n1\n" | gmx trjconv -s md.tpr -f md.xtc -o md_center.xtc -center -pbc mol
